'use strict';

const { GObject } = imports.gi;
const PopupMenu = imports.ui.popupMenu;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const Confirm_Dialog = Me.imports.modules.dialogs.confirm;
const ContainerMenu = Me.imports.modules.containerMenu;

const Docker = Me.imports.lib.docker;
const Tooltip = Me.imports.lib.tooltip;

const Gettext = imports.gettext;
const Domain = Gettext.domain(Me.metadata.uuid);
const _ = Domain.gettext;
const ngettext = Domain.ngettext;

var Container_Menu = GObject.registerClass(
	class Container_Menu extends ContainerMenu.Container_Menu {
		_init(container) {
			super._init(container);

			// Set size of sub menu. !important
			this.menu.actor.style = `min-height: ${container.settings.get_int('submenu-image')}px;`;

			this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem("Docker"));

			switch (container.state) {
				case "running":
					this.new_action_button("utilities-terminal", () => {
						Docker.run_command(Docker.docker_commands.c_exec, container)
					}, _("Exec Bash"));

					this.new_action_button("emblem-symbolic-link", () => {
						Docker.run_command(Docker.docker_commands.c_attach, container)
					}, _("Attach Terminal"));

					this.new_action_button("media-playback-pause", () => {
						Docker.run_command(Docker.docker_commands.c_pause, container)
					}, _("Pause"));

					this.new_action_button("media-playback-stop", () => {
						Docker.run_command(Docker.docker_commands.c_stop, container)
					}, _("Stop"));

					this.new_action_button("object-rotate-left", () => {
						Docker.run_command(Docker.docker_commands.c_restart, container)
					}, _("Restart"));

					break;

				case "paused":
					this.new_action_button("media-playback-start", () => {
						Docker.run_command(Docker.docker_commands.c_unpause, container)
					}, _("Unpause"));

					this.new_action_button("media-playback-stop", () => {
						Docker.run_command(Docker.docker_commands.c_stop, container)
					}, _("Stop"));

					this.new_action_button("object-rotate-left", () => {
						Docker.run_command(Docker.docker_commands.c_restart, container)
					}, _("Restart"));
					break;

				default:
					this.new_action_button("media-playback-start", () => {
						Docker.run_command(Docker.docker_commands.c_start, container)
					}, _("Start"));

					this.new_action_button("utilities-terminal", () => {
						Docker.run_command(Docker.docker_commands.c_start_i, container)
					}, _("Start interactive"));
			}

			this.new_action_button("format-justify-fill", () => {
				Docker.run_command(Docker.docker_commands.c_logs, container)
			}, _("View Logs"));

			this.new_action_button("user-info", () => {
				Docker.run_command(Docker.docker_commands.c_inspect, container)
			}, _("Inspect"));

			// Remove
			this.new_action_button(
				"edit-delete",
				() => Confirm_Dialog.open(
					Docker.docker_commands.c_rm.label, // Title
					`Are you sure you want to ${Docker.docker_commands.c_rm.label}?`, // Description
					() => Docker.run_command(Docker.docker_commands.c_rm, container),
				),
				Docker.docker_commands.c_rm.label // Button
			);

			this.add_compose();
			this.add_ports();
		}

		add_compose() {
			// Check if container belongs to compose.
			if (!this._container.compose_project) {
				return;
			}
			this.buttons = 0;

			this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem(`Compose:${this._container.compose_project}`));

			switch (this._container.state) {
				case "running":
					// Stop
					this.new_action_button("media-playback-stop", () => {
						Docker.run_command(Docker.docker_commands.compose_stop, this._container);
					}, Docker.docker_commands.compose_stop.label);
					break;

				default:
					// Up
					this.new_action_button(
						"media-playback-start",
						() => Docker.run_command(Docker.docker_commands.compose_up, this._container),
						Docker.docker_commands.compose_up.label
					);

					// Remove
					this.new_action_button(
						"edit-delete",
						() => Confirm_Dialog.open(
							Docker.docker_commands.compose_rm.label + this._compose_label, // Dialog title
							`Are you sure you want to ${Docker.docker_commands.compose_rm.label}?`, // Description
							() => Docker.run_command(Docker.docker_commands.compose_rm, container),
						),
						Docker.docker_commands.compose_rm.label // Button label
					);
					break;
			}
		}
	}
)