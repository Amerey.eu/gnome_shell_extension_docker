'use strict';

const { GObject } = imports.gi;
const PopupMenu = imports.ui.popupMenu;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const Confirm_Dialog = Me.imports.modules.dialogs.confirm;
const Me_PopupMenu = Me.imports.modules.popupMenu;

const Docker = Me.imports.lib.docker;
const Tooltip = Me.imports.lib.tooltip;

const Gettext = imports.gettext;
const Domain = Gettext.domain(Me.metadata.uuid);
const _ = Domain.gettext;
const ngettext = Domain.ngettext;


var Image_Menu = GObject.registerClass(
	class Image_Menu extends Me_PopupMenu.PopupSubMenuMenuItem {
		_init(image) {
			super._init(image.name);

			// Set size of sub menu. !important
			this.menu.actor.style = `min-height: ${image.settings.get_int('submenu-image')}px;`;

			this.new_action_button("media-playback-start", () => {
				Docker.run_command(Docker.docker_commands.i_run, image);
			}, _("Run"));

			this.new_action_button("utilities-terminal", () => {
				Docker.run_command(Docker.docker_commands.i_run_i, image);
			}, _("Run interactive"));

			this.new_action_button("user-info", () => {
				Docker.run_command(Docker.docker_commands.i_inspect, image);
			}, _("Inspect"));

			this.new_action_button(
				"edit-delete",
				() => Confirm_Dialog.open(
					Docker.docker_commands.i_rm.label, // Dialog title
					`Are you sure you want to ${Docker.docker_commands.i_rm.label}?`, // Description
					() => Docker.run_command(Docker.docker_commands.i_rm, image),
				),
				Docker.docker_commands.i_rm.label
			);
		}
	}
)